(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name dynamicformsApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the dynamicformsApp
   */
  angular.module('dynamicformsApp')
    .controller('MainCtrl', function ($scope, dynamicformFactory) {
      $scope.forms = dynamicformFactory.formIsaac;
      $scope.modelForm = {};
    });

})();