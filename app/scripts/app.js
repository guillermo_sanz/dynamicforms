(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name dynamicformsApp
   * @description
   * # dynamicformsApp
   *
   * Main module of the application.
   */
  angular
    .module('dynamicformsApp', ['ngMaterial', 'moduleDynamicForms','ngMessages']);

  })()