(function () {
    angular
        .module('moduleDynamicForms')
        .factory('dynamicformFactory', dynamicformFactory);

    function dynamicformFactory() {
        var formIsaac = {
            id: 'I_LAB',
            fields: [
                {
                    "labelClass": "proff-label",
                    "class": "input-wrapper",
                    "element": "select",
                    "fsize": "50",
                    "hideLabel": false,
                    "label": "Servicio",
                    "hideLabel": false,
                    "name": "servicio",
                    "required": true,
                    "type": "select",
                    "order": 2,
                    "options": [
                        {
                            id: 1,
                            value: 'Enlace contable.',
                            selected: false
                        },
                        {
                            id: 2,
                            value: 'Integración',
                            selected: false
                        },
                        {
                            id: 3,
                            value: 'Soporte técnico',
                            selected: false
                        },
                    ]
                },
                {
                    "labelClass": "proff-label",
                    "class": "input-wrapper",
                    "element": "select",
                    "fsize": "50",
                    "hideLabel": false,
                    "label": "Servicio 1",
                    "hideLabel": false,
                    "name": "servicio1",
                    "required": true,
                    "type": "select",
                    "order": 1,
                    "options": [
                        {
                            id: 1,
                            value: 'Enlace contable.',
                            selected: false
                        },
                        {
                            id: 2,
                            value: 'Integración',
                            selected: false
                        },
                        {
                            id: 3,
                            value: 'Soporte técnico',
                            selected: false
                        },
                    ]
                },
                {
                    "labelClass": "proff-label",
                    "class": "input-wrapper",
                    "element": "select",
                    "hideLabel": false,
                    "label": "Servicio2",
                    "hideLabel": false,
                    "fsize": "50",
                    "name": "servicio2",
                    "required": true,
                    "type": "select",
                    "order": 3,
                    "options": [
                        {
                            id: 1,
                            value: 'Enlace contable.',
                            selected: false
                        },
                        {
                            id: 2,
                            value: 'Integración',
                            selected: false
                        },
                        {
                            id: 3,
                            value: 'Soporte técnico',
                            selected: false
                        },
                    ]
                },

            ],
            options: {
                blockClass: 'md-content',
                buttons: [
                    {
                        label: 'Borrar',
                        class: 'bm-btn default',
                        action: 'clear',
                        class: 'md-raised md-primary',
                    },
                    {
                        submit: true,
                        label: 'Guardar',
                        class: 'md-raised md-primary',
                        customDisabled: ''
                    }
                ],
                btnsHiden: true,
                btnClass: 'float-right',
                displayErrors: true
            }

        }
        return {
            formIsaac: formIsaac
        }

    }

})();


