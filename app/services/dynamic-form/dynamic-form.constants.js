(function () {
    angular
        .module('moduleDynamicForms')
        .constant('elements', ['autocomplete', 'button', 'chart', 'checkbox', 'chips', 'date', 'hr', 'input', 'link', 'phoneInput', 'radius', 'radiusBox', 'select', 'selectAsync', 'suggestedCPC', 'switchEl', 'text', 'textarea', 'toogle', 'toggleChips', 'toggleTextarea', 'toolbar'])
})();