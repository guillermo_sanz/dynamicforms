(function () {
    angular
        .module('moduleDynamicForms', [])
        .component('componentDynamicForms', {
            templateUrl: 'services/dynamic-form/dynamic-form.component.html',
            controller: 'ctrlComponentDynamicForms',
            bindings: {
                form: '<',
                model: '<'
            }

        })
        .run(function ($templateCache) {
            $templateCache.put('errors-messages.html',
                `<p ng-message="required">El campo {{field.name}} es obligatorio</p>
            <p ng-message="minlength">El campo {{field.name}} es demasiado corto</p>
            <p ng-message="maxlength">El campo {{field.name}} es demasiado largo</p>
            <p ng-message="email">Debe ser un email correcto </p>`);
        })
        .controller('ctrlComponentDynamicForms', ['elements', '$templateCache', '$templateRequest', '$http', function (elements, $templateCache, $templateRequest, $http) {
            var ctrl = this;
            ctrl.count = 0;
            ctrl.countFileds = 0;
            ctrl.buttonAction = buttonAction;
            ctrl.showErrorsList = showErrorsList;

            ctrl.$onInit = function () {
                //get timestamp
                ctrl.formID = + new Date();                
                createTemplate(ctrl.form)
            };

            function createTemplate(form) {
                var elementContent;
                var fields = form.fields;
                ctrl.countFileds = fields.length;
               
                fields.forEach((field, i)=>{
                    var path = `services/dynamic-form/elements/${field.element}.html`;

                    if (elements.includes(field.element)) {
                        $templateRequest(path).then(function (html) {
                            elementContent = html;
                            if (field.name) {
                                elementContent =
                                    elementContent.replace(/__MODEL__/gi, `modelForm.${field.name}`)
                                elementContent =
                                    elementContent.replace(/__VALIDATION__/gi, bracket(field.name, 'dynamicForm'));
                            }
                            $templateCache.put(`${field.element}_${i}_${ctrl.formID}`, elementContent);
                            ctrl.count++;
                            // if ctrl. count === form.fields.length we show the forms. If we don't wait controller finish put templates in templateCache, we'll have a 404 error. 
                            // This is because we haave to wait al templateRequest promise. The ng-include first search in templateCache, if not founf the key template, search a file template. 
                        });
                    } else {
                        elementContent = 'invalidElement';
                    }
                })
            }

            

            function bracket(model, base) {
                var props = model.split('.');
                return (base || props.shift()) + (props.length ? `.${props.join('\'][\'')}` : '');
                // return (base || props.shift()) + (props.length ? `['${props.join('\'][\'')}']` : '');
            }

            function buttonAction(button) {
                var action = button.action;
                if (button.submit) {
                    button.action($http);
                }
                else {
                    if (typeof action !== 'string') {
                        action();
                    }
                    else {
                        this[action]();
                    }
                }
            }

            function clear() {
                this.vmodel = {};
                this.model = {};
            }

            function submit() {
                this.options.buttons.forEach((button) => {
                    if (button.submit) {
                        button.action();
                    }
                });
            }

            function showErrorsList(dynamicForm) {
                return this.options.displayErrors && dynamicForm.$error.required;
            }
        }])

})();



