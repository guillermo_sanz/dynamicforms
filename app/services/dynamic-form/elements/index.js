import input from './input.html';
import autocomplete from './autocomplete.html';
import hr from './hr.html';
import link from './link.html';
import checkbox from './checkbox.html';
import text from './text.html';
import radius from './radius.html';
import toolbar from './toolbar.html';
import select from './select.html';
import selectAsync from './selectAsync.html';
import toggle from './toggle.html';
import button from './button.html';
import date from './date.html';
import chips from './chips.html';
import switchEl from './switchEl.html';
import textarea from './textarea.html';
import radiusBox from './radiusBox.html';
import toggleChips from './toggleChips.html';
import toggleTextarea from './toggleTextarea.html';
import chart from './chart.html';
import phoneInput from './phoneInput.html';
import suggestedCPC from './suggestedCPC.html';

export default {
  input,
  autocomplete,
  hr,
  link,
  checkbox,
  text,
  radius,
  toolbar,
  select,
  selectAsync,
  toggle,
  button,
  date,
  chips,
  switchEl,
  textarea,
  radiusBox,
  toggleChips,
  toggleTextarea,
  chart,
  suggestedCPC,
  phoneInput
};
